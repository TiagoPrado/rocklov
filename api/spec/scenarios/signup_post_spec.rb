describe "POST /signup" do
    context "novo usuario" do
        before(:all) do
            payload = { name: "Pitty", email: "pitty@bol.com.br", password: "tps123" }
            MongoDB.new.remove_user(payload[:email])

            @result = Signup.new.create(payload)
        end

        it "valida status code" do
            expect(@result.code).to eql 200
        end

        it "valida id do usuario" do
            expect(@result.parsed_response["_id"].length).to eql 24
        end
    end

    context "usuario ja existe" do
        before(:all) do
            # dado que eu tenho um novo usuario
            payload = { name: "João da Silva", email: "joao@ig.com.br", password: "tps123" }
            MongoDB.new.remove_user(payload[:email])

            # e o email desse usuario ja foi cadastrado no sistema
            Signup.new.create(payload)

            # quando faço uma requisição para a rota / signup
            @result = Signup.new.create(payload)
        end

        it "deve retornar 409" do
            # então deve retornar 409 
            expect(@result.code).to eql 409
        end

        it "deve retornar mensagem" do
            expect(@result.parsed_response["error"]).to eql "Email already exists :("
        end
    end

    context "Campo nome obrigatorio" do
        before(:all) do
            # dado que eu tenho um novo usuario
            payload = { email: "rafa@gmail.com", password: "tps123" }
            MongoDB.new.remove_user(payload[:email])

            Signup.new.create(payload)
            @result = Signup.new.create(payload)
        end

        it "deve retornar 412" do
            
            expect(@result.code).to eql 412
        end

        it "deve retornar mensagem required name" do
            expect(@result.parsed_response["error"]).to eql "required name"
        end
    end

    context "Campo email obrigatorio" do
        before(:all) do
            # dado que eu tenho um novo usuario
            payload = { name: "Carolina Prado", password: "tps123" }
            MongoDB.new.remove_user(payload[:email])

            Signup.new.create(payload)
            @result = Signup.new.create(payload)
        end

        it "deve retornar 412" do
            
            expect(@result.code).to eql 412
        end

        it "deve retornar mensagem required email" do
            expect(@result.parsed_response["error"]).to eql "required email"
        end
    end

    context "Campo password obrigatorio" do
        before(:all) do
            # dado que eu tenho um novo usuario
            payload = { name: "Lara Prado", email: "lara@yahoo.com.br" }
            MongoDB.new.remove_user(payload[:email])

            Signup.new.create(payload)
            @result = Signup.new.create(payload)
        end

        it "deve retornar 412" do
            
            expect(@result.code).to eql 412
        end

        it "deve retornar mensagem required password" do
            expect(@result.parsed_response["error"]).to eql "required password"
        end
    end
    
end